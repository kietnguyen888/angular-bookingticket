import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  // @Output() eventChangeBookedList = new EventEmitter()

  listChange: Seat[] = [];

  changeBookedListParent(receivesList: []) {
    this.listChange = receivesList;
    // console.log(this.listChange);
  }
  
  constructor() {}

  ngOnInit(): void {}
}
interface Seat {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
